#pragma once
#include <string>
#include "Bibikov.h"
#include "Zangeneh.h"

using namespace std;

class menu
{
	static const int menu_items_number = 9;
	string menu_items[menu_items_number] = { "1. Create product list", "2. Add product to list", "3. Display product list",
		"4. Create product list file", "5. Add product to product list file", "6. Display product list file",
		"7. Add product to the buyer's shopping list", "8. Delete product from the buyer's shopping list",
		"9. Display buyer's shopping list" };

public:
	void list_creation(products_list& new_list);

	void add_to_list(products_list& list);

	void list_display(products_list list);

	void file_creation(file& new_file, products_list list);

	void add_to_file(file& current_file);

	void file_display(file current_file);

	void append_shopping_list(buyer& shopping_list, products_list list);

	void remove_from_shopping_list(buyer& shopping_list, products_list list);

	void display_shopping_list(buyer shopping_list);

	void display_menu();
};