#include <iostream>
#include "Products.h"
#include "Bibikov.h"
#include "Zangeneh.h"
#include "Menu.h"

using namespace std;

void main()
{
	extern int choise;
	menu main_menu;
	products_list main_list;
	file main_list_file;
	buyer buyer_1;

	while (1) {
		main_menu.display_menu();

		switch (choise)
		{
		case 1: main_menu.list_creation(main_list); break;
		case 2: main_menu.add_to_list(main_list); break;
		case 3: main_menu.list_display(main_list); break;
		case 4: main_menu.file_creation(main_list_file, main_list); break;
		case 5: main_menu.add_to_file(main_list_file); break;
		case 6: main_menu.file_display(main_list_file); break;
		case 7: main_menu.append_shopping_list(buyer_1, main_list); break;
		case 8: main_menu.remove_from_shopping_list(buyer_1, main_list); break;
		case 9: main_menu.display_shopping_list(buyer_1); break;
		case 0: exit(0);
		}
	}
}