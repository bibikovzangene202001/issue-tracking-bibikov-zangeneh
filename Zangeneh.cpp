#include "Zangeneh.h"
#include <iostream>

void buyer::buy(products new_purchased)
{
	purchased_item* new_item = new purchased_item;
	new_item->item = new_purchased;
	if (list_head == nullptr)
	{
		list_head = new_item;
	}
	if (list_tail == nullptr)
	{
		list_tail = new_item;
	}
	else
	{
		list_tail->next = new_item;
		new_item->prev = list_tail;
		list_tail = new_item;
	}
}

void buyer::delete_product(products purchase_remove)
{
	purchased_item* current_item = list_head;
	while (current_item != nullptr) {
		if (current_item->item.id == purchase_remove.id)
		{
			if ((current_item->next == nullptr) && (current_item->prev == nullptr))
			{
				delete current_item;
				list_head = nullptr;
				list_tail = nullptr;
				break;
			}
			else if (current_item->prev == nullptr)
			{
				current_item->next->prev = current_item->prev;
				list_head = current_item->next;
				delete current_item;
				break;
			}
			else if (current_item->next == nullptr)
			{
				current_item->prev->next = current_item->next;
				list_tail = current_item->prev;
				delete current_item;
				break;
			}
			else
			{
				current_item->prev->next = current_item->next;
				current_item->next->prev = current_item->prev;
				delete current_item;
				break;
			}
		}
		current_item = current_item->next;
	}
}

void buyer::display_purchased()
{
	int total_cost = 0;
	purchased_item* current_item = list_head;
	while (current_item != nullptr) {
		cout << "\nid: " << current_item->item.id << endl;
		cout << "name: " << current_item->item.name << endl;
		cout << "price: " << current_item->item.price << endl;
		total_cost += current_item->item.price;
		current_item = current_item->next;
	}

	cout << "\nThe total cost of the purchased goods is: " << total_cost << endl;
}
