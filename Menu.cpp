#include "Menu.h"
#include "Products.h"
#include <iostream>

int choise = 0;

void menu::list_creation(products_list& new_list)
{
	system("cls");

	cout << "Product list creation:" << endl;
	int list_items_number = 0;
	cout << "Enter the amount of the items in the list please" << endl;
	cout << "amount: ";
	cin >> list_items_number;
	CLEAR_INPUT;
	new_list.create(list_items_number);

	cout << "\nThe list of " << list_items_number << " elements has been successfully created" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::add_to_list(products_list& list)
{
	system("cls");

	cout << "Element addition to the product list" << endl;
	cout << "Enter the product's id, please" << endl;
	cout << "id: ";
	int item_id = 0;
	cin >> item_id;
	CLEAR_INPUT;
	cout << "Enter the product's name, please" << endl;
	cout << "name: ";
	string item_name = "";
	getline(cin, item_name);
	cout << "Enter the product's price, please" << endl;
	cout << "price: ";
	int item_price = 0;
	cin >> item_price;
	CLEAR_INPUT;

	list.add(item_id, item_name, item_price);

	cout << "\nThe product has been successfully added to list" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::list_display(products_list list)
{
	system("cls");
	cout << "Product list:" << endl;

	list.display();

	cout << "\nThe list has been successfully displayed" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::file_creation(file& new_file, products_list list)
{
	system("cls");

	cout << "Product list file creation" << endl;
	cout << "Enter the name of the file, please" << endl;
	string file_name = "";
	getline(cin, file_name);

	new_file.create(file_name, list);

	cout << "\nThe products list file has been successfully created" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::add_to_file(file& current_file)
{
	system("cls");

	cout << "Element addition to the product list file" << endl;
	products new_product;
	cout << "Enter the product's id, please" << endl;
	cin >> new_product.id;
	CLEAR_INPUT;
	cout << "Enter the product's name, please" << endl;
	getline(cin, new_product.name);
	cout << "Enter the product's price, please" << endl;
	cin >> new_product.price;
	CLEAR_INPUT;

	current_file.add(new_product);

	cout << "\nThe product was successfully added to file" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::file_display(file current_file)
{
	system("cls");

	cout << "Products list file:" << endl;
	current_file.display();

	cout << "\nThe products list file has been successfully displayed" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::append_shopping_list(buyer& shopping_list, products_list list)
{
	system("cls");

	cout << "Appending the buyer's shopping list" << endl;
	cout << "Enter the id of the purchased product, please" << endl;
	int item_id = 0;
	cout << "id: ";
	cin >> item_id;
	CLEAR_INPUT;

	products* purchased_item = list.get_head();
	while ((purchased_item != nullptr) && (purchased_item->id != item_id)) {
		purchased_item = purchased_item->next;
	}
	if (purchased_item == nullptr)
	{
		cout << "\nThe product with specified id does not exis" << endl;
	}
	else
	{
		shopping_list.buy(*purchased_item);
		cout << "\nThe product was successfully added to buyer's shopping list" << endl;
	}
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::remove_from_shopping_list(buyer& shopping_list, products_list list)
{
	system("cls");

	cout << "Deletion from shopping list" << endl;
	cout << "Enter the id of the product that to remove, please" << endl;
	int item_id = 0;
	cout << "id: ";
	cin >> item_id;
	CLEAR_INPUT;

	products* purchased_item = list.get_head();
	while ((purchased_item != nullptr) && (purchased_item->id != item_id)) {
		purchased_item = purchased_item->next;
	}
	if (purchased_item == nullptr)
	{
		cout << "\nThe product with specified id does not exis" << endl;
	}
	else
	{
		shopping_list.delete_product(*purchased_item);
		cout << "\nThe product was successfully removed from the buyer's shopping list" << endl;
	}
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::display_shopping_list(buyer shopping_list)
{
	system("cls");

	cout << "Buyer's shopping list:" << endl;

	shopping_list.display_purchased();

	cout << "\nThe buyer's shopping list has been successfully displayed" << endl;
	cout << "Press Enter to return to menu, please" << endl;
	char key = '\0';
	while (key != '\n') {
		key = getchar();
	}
}

void menu::display_menu()
{
	system("cls");
	choise = 0;

	cout << "Menu:" << endl;
	for (int counter = 0; counter < menu_items_number; counter++) {
		cout << menu_items[counter] << endl;
	}
	cout << "\nEnter the number of operation you wnat to choose, or enter 0 to exit program, please" << endl;
	cout << "Number: ";
	cin >> choise;
	CLEAR_INPUT;
}
