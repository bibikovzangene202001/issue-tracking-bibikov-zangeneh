#pragma once
#include "Products.h"

using namespace std;

class products_list
{
	products* list_head = nullptr;
	products* list_tail = nullptr;

public:
	void add(int item_id, string item_name, int item_price);

	void create(int items_amount);
	
	void display();

	products* get_head() { return list_head; }
};

class file
{
	string file_name = "";

public:
	void create(string new_file_name, products_list list);
	
	void add(products product_item);
	
	void display();
};