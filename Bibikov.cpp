#include "Bibikov.h"
#include "Products.h"
#include <iostream>
#include <fstream>

void products_list::add(int item_id, string item_name, int item_price)
{
	products* new_item = new products;
	new_item->id = item_id;
	new_item->name = item_name;
	new_item->price = item_price;

	if (list_head == nullptr)
	{
		list_head = new_item;
	}

	if (list_tail == nullptr)
	{
		list_tail = new_item;
	}
	else
	{
		list_tail->next = new_item;
		list_tail = new_item;
	}
}

void products_list::create(int items_amount)
{
	for (int counter = 0; counter < items_amount; counter++) {
		int product_id = 0;
		string product_name = "";
		int product_price = 0;

		cout << "\nEnter the product id, please" << endl;
		cout << "id: ";
		cin >> product_id;
		CLEAR_INPUT;
		cout << "Enter the product name, please" << endl;
		cout << "name: ";
		getline(cin, product_name);
		cout << "Enter the product price, please" << endl;
		cout << "price: ";
		cin >> product_price;
		CLEAR_INPUT;

		add(product_id, product_name, product_price);
	}
}

void products_list::display()
{
	products* product_item = list_head;
	while (product_item != nullptr) {
		cout << "\nid: " << product_item->id << endl;
		cout << "name: " << product_item->name << endl;
		cout << "price: " << product_item->price << endl;
		product_item = product_item->next;
	}
}

void file::create(string new_file_name, products_list list)
{
	new_file_name += ".txt";
	file_name = new_file_name;
	ofstream fout(file_name);
	fout.close();

	products* product_item = list.get_head();
	while (product_item != nullptr) {
		add(*product_item);
		product_item = product_item->next;
	}
}

void file::add(products product_item)
{
	fstream fout(file_name, ios::app);
	fout << product_item.id << endl;
	fout << product_item.name << endl;
	fout << product_item.price << endl;
	fout.close();
}

void file::display()
{
	ifstream fin(file_name);
	string id_sentence = "";
	while (getline(fin, id_sentence)) {
		cout << "\nid: " << id_sentence << endl;
		string name_sentence = "";
		getline(fin, name_sentence);
		cout << "name: " << name_sentence << endl;;
		string price_sentence = "";
		getline(fin, price_sentence);
		cout << "price: " << price_sentence << endl;
	}

	fin.close();
}
