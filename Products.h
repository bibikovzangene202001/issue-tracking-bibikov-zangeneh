#pragma once
#include <string>
#define CLEAR_INPUT cin.clear(); while (cin.get() != '\n');

using namespace std;

struct products
{
	int id = 0;
	string name = "";
	int price = 0;
	products* next = nullptr;
};