#pragma once
#include "Products.h"

class buyer
{
	struct purchased_item
	{
		products item;
		purchased_item* next = nullptr;
		purchased_item* prev = nullptr;
	};

	purchased_item* list_head = nullptr;
	purchased_item* list_tail = nullptr;

public:
	void buy(products new_purchased);
	
	void delete_product(products purchase_remove);

	void display_purchased();
};